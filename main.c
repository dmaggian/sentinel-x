/**
  Generated main.c file from MPLAB Code Configurator

  @Company
    Microchip Technology Inc.

  @File Name
    main.c

  @Summary
    This is the generated main.c using PIC24 / dsPIC33 / PIC32MM MCUs.

  @Description
    This source file provides main entry point for system initialization and application code development.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.170.0
        Device            :  PIC24FJ128GA204
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.61
        MPLAB 	          :  MPLAB X v5.45
*/

/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/
#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/system.h"
#include <stdio.h>
#include <string.h>
#include "ms5607.h"
#include "bit.h"

#define CONSOLE_OUTPUT 1
#define FCY 4000000UL
//#define FCY (_XTAL_FREQ/2)
#include <libpic30.h>

typedef enum {
 
  STATE_STARTUP,
  STATE_CALIBRATION,
  STATE_MONITOR,
  STATE_ALARM

} SENTINEL_STATES;

//Variables for this module
//uint8_t initialzied = 0;
uint16_t cnt = 0;
double temperature = 24;
double pressure = 900;
uint8_t sentinel_state = STATE_STARTUP;
uint8_t calibrated = 0;

#ifdef CONSOLE_OUTPUT
//Function prototype for screen output if CONSOLE_OUTPUT defined
void console_output(double temperature, double pressure,int adc_value );
#endif

/*
                         Main application
 */
int main(void)
{
    // initialize the device
    SYSTEM_Initialize();
    uint8_t flash_cnt = 0;
    
    while (1)
    {

        int conversion,i=0;

        switch(sentinel_state) {

            case STATE_STARTUP:
                if(calibrated) {
                    sentinel_state = STATE_MONITOR;
                } else {
                    //Flash/Sound led/ buzzer 3 times for .5 seconds
                    for(flash_cnt = 0;flash_cnt <= 4; flash_cnt++) {
                        BUZZER_Toggle();
                        LED_Toggle();
                        __delay_ms(500);
                    }
                    LED_SetHigh();
                    BUZZER_SetLow();
                    
                    //Reset the MS5607 and read the calibration values
                    ms5607_reset();
                    ms5607_read_prom();
                    
                    //Initialize the adc
                    ADC1_Initialize();
                    
                    //Goto the calibration state
                    calibrated = 1;
                    sentinel_state = STATE_CALIBRATION;
                }    
                break;
            case STATE_CALIBRATION:
                sentinel_state = STATE_MONITOR;
                break;
            case STATE_MONITOR:
                break;
        }

        ADC1_Enable();
        ADC1_ChannelSelect(channel_AN10);
        ADC1_SoftwareTriggerEnable();
        //Provide Delay
        for(i=0;i <1000;i++)
        {
        }
        ADC1_SoftwareTriggerDisable();
        while(!ADC1_IsConversionComplete(channel_AN10));
        conversion = ADC1_ConversionResultGet(channel_AN10);
        ADC1_Disable(); 

        temperature =  ms5607_read_temp(MS5607_ConvertD2_4096);
        pressure = ms5607_read_press(MS5607_ConvertD1_4096);        

#ifdef CONSOLE_OUTPUT
        console_output(temperature,pressure,conversion);
#endif
        __delay_ms(100);

    }

    return 1;
}

#ifdef CONSOLE_OUTPUT
void console_output(double temperature, double pressure,int adc_value ) {

    double inHg;
    static int screen_init = 0;
    uint8_t a;
    
    //If it's the fist time through, clear the screen
    //Clear the screen and hide the cursor
    if(!screen_init) {
        printf("\x1b[2J");  //Clear screen
        printf("\e[?25l");  //Hide cursor
        printf("\x1b[32m"); //Set text color to green
        
        screen_init = 1;
    }
    
    //Clear the screen and the current line 
    //since ADC readings vary in length
    printf("\x1b[%d;%df", 1, 10);
    printf("\x1b[1J");
    printf("\x1b[%d;%df", 1, 1);
    printf("ADC = %d\r\n", adc_value);
    printf("Temp/Press conversions %d\r\n",cnt++);
    printf("Temperature = %.2f\370 C\r\n", temperature);

    pressure = ms5607_read_press(MS5607_ConvertD1_4096);        
    inHg = pressure * 0.0295301;
    printf("Pressure = %.2f millibars, %.2f (inHg)\r\n", pressure, inHg);
    a = read_BITcal();
    printf("BITcal = %3d ", a);

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 

    printf(BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(a));        
    printf("\r\n");
    // Add your application code

    
}
#endif
/**
 End of File
*/

