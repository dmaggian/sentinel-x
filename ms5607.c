/*
 File: ms5607.c
 * Description:
 *      This file contains helper functions to read temperature and pressure
 *      values along with the calibration values stored in prom that are
 *      used to compensate the measurements
 */

#include <stdio.h>
#include "mcc_generated_files/mcc.h"
#include "ms5607.h"
#include "math.h"

//#define FCY 4000000UL
#define FCY (_XTAL_FREQ/2)
#include <libpic30.h>

// Variable for this module)

uint32_t D1,D2 = 0x12345;
double T, T2, P, SENS, SENS2, OFF, OFF2, dT;

union ArrayToInteger {
  uint8_t array[4];
  uint32_t integer;
};

uint8_t initialized = 0;
uint8_t buf[20];
uint16_t reg_val;
uint16_t c[8];

void ms5607_reset(void) {

    //Reset the ms5607
    buf[0] = MS5607_RESET;
    i2c_writeNBytes(MS5607_ADDRESS, buf, 1);
    __delay_ms(5);
    
}

void ms5607_read_prom(void) {

    //Send read prom command followed a read of two bytes 
    buf[0] = MS5607_PROM_READ | 0x01;
    //i2c_writeNBytes(MS5607_ADDRESS, buf, 1);

    // Now read the data
    //i2c_readNBytes(MS5607_ADDRESS, &val, 2);
    c[0] = i2c_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x00);
    __delay_ms(1);
    
    c[1] = i2c_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x02);
    __delay_ms(1);
    c[2] = i2c_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x04);
    __delay_ms(1);
    c[3] = i2c_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x06);
    __delay_ms(1);
    c[4] = i2c_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x08);
    __delay_ms(1);
    c[5] = i2c_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x0a);
    __delay_ms(1000);
    c[6] = i2c_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x0c);
    __delay_ms(1);
    c[7] = i2c_read2ByteRegister(MS5607_ADDRESS, MS5607_PROM_READ | 0x0e);
    __delay_ms(1);
    
}

double ms5607_read_temp(uint8_t cmd) {
  
    //Trigger the conversion
    buf[0] = cmd;
    i2c_writeNBytes(MS5607_ADDRESS, buf, 1);

    //Wait the appropriate delay time based on the over sampling rate
    switch (cmd)
    {
        case MS5607_ConvertD2_256 :  __delay_ms(1);
            break;
        case MS5607_ConvertD2_512 :  __delay_ms(3);
            break;
        case MS5607_ConvertD2_1024 : __delay_ms(4);
            break;
        case MS5607_ConvertD2_2048 : __delay_ms(6);
            break;
        case MS5607_ConvertD2_4096 : __delay_ms(10);
            break;
        default:
        break;
  }    
    buf[0] = MS5607_ADC_Read;
    i2c_writeNBytes(MS5607_ADDRESS, buf, 1);
    __delay_ms(1);

    //Read the data
    i2c_readNBytes(MS5607_ADDRESS, &buf[1] , 3);
    __delay_ms(1);

    //printf("buff[0] = %x\r\n",buf[1]);
    //printf("buff[1] = %x\r\n",buf[2]);
    //printf("buff[3] = %x\r\n",buf[3]);
    //D2 = 0x8183e0;

    //Build long int from the bytes read
    D2 = (uint32_t) 0 << 24;
    D2 |=  (uint32_t) buf[1] << 16;
    D2 |= (uint32_t) buf[2] << 8;
    D2 |= (uint32_t) buf[3];    
    
     // calculate 1st order temperature conversion according to data sheet
    dT= D2 - c[5] * pow(2,8);
    T=(2000+(dT*c[6]) / pow(2,23))/100;
    
    // perform higher order correction if temperature < 20 degrees c
	//double T2=0., OFF2=0., SENS2=0.;
	if(T<2000) {
	  T2 = dT * dT / pow(2,31);
	  OFF2 = 61 * ((T-2000) * (T-2000)) / pow(2,4);
	  SENS2 = 2 * ((T-2000) * (T-2000));
	  if(T < -1500) {
	    OFF2 = OFF2 + 15 * ((T + 1500) * (T + 1500));
	    SENS2 = SENS2 + 8 * ((T +1500) * (T +1500));
	  }
	}
	
    return T;
}

double ms5607_read_press(uint8_t cmd) {

    //Trigger the conversion
    buf[0] = MS5607_ConvertD1_256;
    i2c_writeNBytes(MS5607_ADDRESS, buf, 1);
    
    //Wait the appropriate delay time based on the over sampling rate
    switch (cmd)
    {
        case MS5607_ConvertD1_256 :  __delay_ms(1);
            break;
        case MS5607_ConvertD1_512 :  __delay_ms(3);
            break;
        case MS5607_ConvertD1_1024 : __delay_ms(4);
            break;
        case MS5607_ConvertD1_2048 : __delay_ms(6);
            break;
        case MS5607_ConvertD1_4096 : __delay_ms(10);
            break;
        default:
        break;
  }    

    buf[0] = MS5607_ADC_Read;
    i2c_writeNBytes(MS5607_ADDRESS, buf, 1);
    __delay_ms(1);

    //Read the data
    i2c_readNBytes(MS5607_ADDRESS, &buf[1], 3);
    __delay_ms(1);
    //printf("buff[0] = %x\r\n",buf[1]);
    //printf("buff[1] = %x\r\n",buf[2]);
    //printf("buff[3] = %x\r\n",buf[3]);

    //Build long int from the bytes read
    D1 = (uint32_t) 0 << 24;
    D1 |=  (uint32_t) buf[1] << 16;
    D1 |= (uint32_t) buf[2] << 8;
    D1 |= (uint32_t) buf[3]; 
    P = 0;

    OFF=c[2]*pow(2,17)+dT*c[4]/pow(2,6);
    SENS=c[1]*pow(2,16)+dT*c[3]/pow(2,7);
    P=(((D1*SENS)/pow(2,21)-OFF)/pow(2,15))/100; 

    return P;

}


