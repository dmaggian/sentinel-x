#include "mcc_generated_files/system.h"
#include "bit.h"

/*
 * Function to write non contiguous bits as a register
void set_BITout(uint8_t a) 
 { 
   PORTHbits.RH7 = a & 0x1;
   PORTFbits.RF3 = (a & 0x2) != 0; 
   PORTFbits.RF4 = (a & 0x4) != 0;
   PORTFbits.RF4 = (a & 0x8) != 0;
   PORTFbits.RF4 = (a & 0x10) != 0;
   PORTFbits.RF4 = (a & 0x20) != 0;
   PORTFbits.RF4 = (a & 0x40) != 0;

 }
 */

// Function to read BITcal inputs
uint8_t read_BITcal(void) {
    uint8_t a = 0;
    
    //Read Bits from MSB to LSB
    if(0 == BITinBit6) { a &= ~(1 << 6); } else { a |= 1 << 6; }
    if(0 == BITinBit5) { a &= ~(1 << 5); } else { a |= 1 << 5; }
    if(0 == BITinBit4) { a &= ~(1 << 4); } else { a |= 1 << 4; }
    //if(0 == BITinBit3) { a &= ~(1 << 3); } else { a |= 1 << 3; }
    //if(0 == BITinBit2) { a &= ~(1 << 2); } else { a |= 1 << 2; }
    //if(0 == BITinBit1) { a &= ~(1 << 1); } else { a |= 1 << 1; }
    //if(0 == BITinBit0) { a &= 1; } else { a |= 1; }

    return a;
}    

