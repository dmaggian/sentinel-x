/* 
 * File:   ms5607.h
 * Author: David Maggiano
 *  Header file for the MS5607 helper functions
 * Created on July 9, 2021, 12:56 PM
 */

#ifndef MS5607_H
#define	MS5607_H

#define reverse_bytes_32(num) ( ((num & 0xFF000000) >> 24) | ((num & 0x00FF0000) >> 8) | ((num & 0x0000FF00) << 8) | ((num & 0x000000FF) << 24) )

#define MS5607_ADDRESS  0x77   //I2C address

#define MS5607_RESET            0x1E   
#define MS5607_ConvertD1_256    0x40    //Convert pressure oversample = 256
#define MS5607_ConvertD1_512    0x42    //Convert pressure oversample = 512
#define MS5607_ConvertD1_1024   0x44    //Convert pressure oversample = 1024
#define MS5607_ConvertD1_2048   0x46    //Convert pressure oversample = 2048
#define MS5607_ConvertD1_4096   0x48    //Convert pressure oversample = 4096
#define MS5607_ConvertD2_256    0x50    //Convert temperature oversample = 256
#define MS5607_ConvertD2_512    0x52    //Convert temperature oversample = 512
#define MS5607_ConvertD2_1024   0x54    //Convert temperature oversample = 1024
#define MS5607_ConvertD2_2048   0x56    //Convert temperature oversample = 2048
#define MS5607_ConvertD2_4096   0x58    //Convert temperature oversample = 4096
#define MS5607_ADC_Read         0x00    //Trigger ADC read
#define MS5607_PROM_READ        0xA0    //Read calibration values from Prom

/*
#define MS5607_ADC_READ     0x00  // ADC read command 
#define MS5607_ADC_CONV     0x40  // ADC conversion command
#define MS5607_ADC_D1       0x00  // ADC D1 conversion 
#define MS5607_ADC_D2       0x10  // ADC D2 conversion 
#define MS5607_ADC_256      0x00  // ADC OSR=256 
#define MS5607_ADC_512      0x02  // ADC OSR=512 
#define MS5607_ADC_1024     0x04  // ADC OSR=1024 
#define MS5607_ADC_2048     0x06  // ADC OSR=2056 
#define MS5607_ADC_4096     0x08  // ADC OSR=4096 
*/

//Function Prototypes
void   ms5607_reset(void);
void   ms5607_read_prom(void);
double ms5607_read_press(uint8_t cmd);
double ms5607_read_temp(uint8_t cmd);


#ifdef	__cplusplus
extern "C" {
#endif



#ifdef	__cplusplus
}
#endif

#endif	/* MS5607_H */

