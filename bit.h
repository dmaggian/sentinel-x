/* 
 * File:   bit.h
 * Author: Dave
 *
 * Created on July 15, 2021, 8:01 PM
 */

#ifndef BIT_H
#define	BIT_H

#ifdef	__cplusplus
extern "C" {
#endif

#define BITinBit0
#define BITinBit1 
#define BITinBit2 
#define BITinBit3 
#define BITinBit4 PORTAbits.RA8
#define BITinBit5 PORTAbits.RA7
#define BITinBit6 PORTAbits.RA2
#define BITinBit7 

#define BIToutBit1 
#define BIToutBit2 
#define BIToutBit3 
#define BIToutBit4 
#define BIToutBit5 
#define BIToutBit6 
#define BIToutBit7 
    
//Function prototypes    
uint8_t read_BITcal(void);
void write_BITout(uint8_t a);

#ifdef	__cplusplus
}
#endif

#endif	/* BIT_H */

